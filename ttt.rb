# h/t Chuck Bagget https://mobile.twitter.com/ChuckSciTech/status/1281692052527419396

class TicTacToe

  def dun?(b, p)
    # puts "dun?(#{b}, '#{p}')"
    return true unless b.flatten.include? ' '
    [
      [b[0][0], b[0][1], b[0][2]],
      [b[1][0], b[1][1], b[1][2]],
      [b[2][0], b[2][1], b[2][2]],
      [b[0][0], b[1][0], b[2][0]],
      [b[0][1], b[1][1], b[2][1]],
      [b[0][2], b[1][2], b[2][2]],
      [b[0][0], b[1][1], b[2][2]],
      [b[0][2], b[1][1], b[2][0]]
    ].include? [p, p, p]
  end

  def alternate(player)
    player == 'X' ? 'O' : 'X'
  end

  def myclone(a, m, n, p)
    clone = Array.new(3) do |i|
      Array.new(3) {|j| a[i][j]}
    end
    clone[m][n] = p
    # puts "#{a}->#{clone}"
    clone
  end

  def branches(state, player)
    clones = []
    (0..2).each do |i|
      (0..2).each do |j|
        # puts "#{i}, #{j}"
        if state[i][j] == ' '
          clones << myclone(state, i, j, player)
        end
      end
    end
    # puts "#{clones}"
    # puts "stalemate" if clones.empty?
    clones
  end

  def analyze(state, player)
    # puts "#{player} to play #{state}"
    # dup = nil
    branches(state, player).each do |b|
      @boards << b
      @boardcount += 1
      if @boardcount > 19683
        @boards.uniq!
        puts "#{@boardcount}"
        @boardcount = 0
      end
      # puts v if u != v
      if dun?(b, player)
        @endstates << b
      else
        analyze(b, alternate(player))
      end
    end
  end

  def initialize
    @board = Array.new(3, Array.new(3, ' '))
    @boards = []
    @endstates = []
    @boardcount = 0
    self.analyze(@board, 'X')
    puts "#{@endstates.uniq.count} distinct end states, #{@boards.uniq.count} distinct in-game states"
  end

end

TicTacToe.new